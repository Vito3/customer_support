<%--
  Created by IntelliJ IDEA.
  User: vito
  Date: 03.11.16.
  Time: 10:51
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>

<div class="alert alert-success fade in">

    <a href="#" class="close" data-dismiss="alert">&times;</a>
    <strong>Success!</strong> Your message has been sent successfully. Someone will contact You shortly. If You wish to send another, click below.<br> <br>
    <g:link class="btn btn-purple" controller="form" action="index">New ticket</g:link>
    %{--<button class="btn btn-primary">New ticket</button>--}%
</div>

</body>
</html>