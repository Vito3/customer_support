<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container-fluid">

    <g:render template="/common/ajaxError"></g:render>

     <g:form controller="form" action="sendEmail">
         <form id="fr1" class="form-inline">
         <div class="form-group">
             <label class='control-label' for="productID">Product:</label><br>
             <g:textField name="product" type="text" class="form-control" id="productID"  required="required"/><br>
             <label class='control-label'  for="userID">User:</label><br>
             <g:textField name="userMail" type="text" class="form-control" id="userID"  required="required"/>
             <br>
             <br>

             <br>
             <label class='control-label' for="comment">Description:</label>
                <g:textArea  name="mailContent" class="form-control"  rows="5" id="comment"  value="Explain your issue in detail..." required="required"/>
             <br>
             <br>
             <input id="uploadFile" placeholder="Choose File" disabled="disabled" />
         <div class="fileUpload btn btn-purple">
             <span>Upload</span>
             <input id="uploadBtn" type="file" class="upload" />
         </div>
         <br>
         <br>
         <g:submitButton class="btn btn-purple" name="sendData" value="Submit" id="submit"></g:submitButton>
         <br>
         <br>


 </div>
 </form>


</g:form>
%{--<form id="fr1" class="form-inline">--}%
        %{--<div class="form-group required">--}%
            %{--<label class='control-label' for="productID">Product:</label><br>--}%
            %{--<input type="text" class="form-control" id="productID" disabled ><br>--}%
            %{--<label class='control-label'  for="userID">User:</label><br>--}%
            %{--<input type="text" class="form-control" id="userID" disabled >--}%
            %{--<br>--}%
            %{--<br>--}%
            %{--<label class='control-label' for="titleID">Title:</label><br>--}%
            %{--<input type="text" class="form-control" name="title" id="titleID"  >--}%

            %{--<label class='control-label' for="comment">Description:</label>--}%
            %{--<textarea class="form-control"  rows="5" id="comment"  value="Explain your issue in detail..."></textarea>--}%
        %{--</div>--}%



    %{--</form>--}%
    %{--<br>--}%
    %{--<div class="form-group required">--}%

    %{--</div>--}%
    %{--<br>--}%
    %{--<button type="submit" id="submitID" class="btn btn-primary">Submit</button>--}%
    %{--<br><br>--}%

    %{--<form action="/upload-target" class="dropzone"></form>--}%
    %{--<br><br>--}%
    %{--<div class="alert alert-danger fade in">--}%
        %{--<a href="#"  id="alert" class="close" data-dismiss="alert" aria-label="close">&times;</a>--}%
        %{--<strong>Warning!</strong> All fields are required.--}%


        %{--<br>--}%
        %{--<br>--}%
        %{--<font size="2" color="red"><i>* - This is a required field.</i></font>--}%
    %{--</div>--}%
</div>

<script>
    document.getElementById("uploadBtn").onchange = function () {
        document.getElementById("uploadFile").value = this.value;
    };



</script>
</body>
<script src ="https://code.jquery.com/jquery-2.2.4.min.js"></script>
<script src ="cs_script.js"></script>


<style>
.form-group.required .control-label:after {
    content:"*";
    color:red;
}

</style>
</html>