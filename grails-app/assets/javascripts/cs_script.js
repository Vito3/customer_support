$(document).ready(function() {



	function getUrlQueryParams()
	{
	    var queryParams = {}, param;

	    var params = window.location.search.substring(1).split("&");
	    for(var i = 0; i < params.length; i++)
	    {
	        param = params[i].split('=');
	        queryParams[param[0]]=param[1];
	    }
	    return params;
	}
	 
	var queryParams = getUrlQueryParams();



    if(queryParams!=""){
        var userId=queryParams[1].split('=');
        var productName = queryParams[0].split('=');
		$('#productID').val(productName[1]);
		$('#userID').val(userId[1]);
		$("#productID").prop('readonly', true);
		$("#userID").prop('readonly', true);
	}
	else if(queryParams==""){
		$("#productID").prop('readonly', false);
		$("#userID").prop('readonly', false);
	}


});


